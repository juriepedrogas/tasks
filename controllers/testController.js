const axios = require('axios')
const helper = require("../helper/helper.js")
const service1 = require("../services/task1.js")
const url = require('../constants/url.js')
require('dotenv').config()

async function transform(ctx){
    //Determine Sports
    if(!determineSport(ctx.params.sportName).found) ctx.throw(404,'Not Found')

        const inPlayFilterResults = await axios.get(`${url.inPlayFilter}&sport_id=${determineSport(ctx.params.sportName).sportId}`)
        let inPlayFilter = inPlayFilterResults.data.results
        inPlayFilter = inPlayFilter//.slice(0, inPlayFilter?.length - (inPlayFilter?.length - 1))

        const inPlayResults = await axios.get(url.inPlay)
        let inPlay = await service1.transform(inPlayResults?.data.results[0])
        

        const sport = inPlay?.find((ival)=> ival.type === 'CLASSIFICATION' && ival.ID === determineSport(ctx.params.sportName).sportId.toString())

        let inPlayFilterWithCompleteDetails = await Promise.all(inPlayFilter.map(async (val)=>{

            const inPlayEventDetailsResults = await axios.get(`${url.inPlayEvent}&FI=${val.id}`) 
            let inPlayEventDetails = inPlayEventDetailsResults.data.results ? await service1.transform(inPlayEventDetailsResults.data?.results[0]) : []
            
            let filteredInPlay = inPlay?.find((fval)=> fval.type === 'EVENT' && fval.ID.includes(val.r_id))
            
            return{
                ...val,
                sport:sport.NAME,
                events:{stats:inPlayResults?.data.stats,results:await filteredInPlay},
                details:inPlayEventDetails,
                
            }
        }))

        // let results = restructureJSON(inPlayFilterWithCompleteDetails)
        
        // let obj = {}

        // results.map((val)=>{
        //     let objKey = Object.keys(val)[0]
        //     obj[objKey] = val[objKey]
        // })
        // const ts = Math.floor(new Date().getTime() / 1000)
        // let result = {
        //     bm:"bet365",
        //     updated:UnixToDate(ts),
        //     updated_ts:ts,
        //     events:obj}

        ctx.body = helper.prettify(inPlayFilterWithCompleteDetails)

}

function getID(str){
    let result = str.split("C")[0]

    return result
}

function restructureJSON(data){
    const result = data.map((val)=>{
        //let bet365id = val.ev_id.slice(2).slice(0,-3)

        const teamGroup = val.details.filter((v)=> v.type === 'TEAM_GROUP')
        const homeTeam = val.details.filter((v)=> v.type === 'TEAM_GROUP' && v.OR === '0')
        const awayTeam = val.details.filter((v)=> v.type === 'TEAM_GROUP' && v.OR === '1')
        const stats = val.details.filter((v)=> v.type === 'SCORE, SCORES_COLUMN' || v.type === 'SCORES_CELL')
        const extra = val.details.filter((v)=> v.type === 'INFO_POD_DETAIL_1, STAT, POD_BODY_TEXT_1, STAKE' && v['LABEL, INFO_POD_LINK_1_ID'])

        return{
            [val.details['C2_ID, MINI_DIARY_C2']]:{
                core:{
                    safe: "",
                    removed: "",
                    stopped: "0",
                    blocked: "0",
                    finished: "0",
                    nc: "0",
                    updated: UnixToDate(val.updated_at),
                    updated_ts: val.updated_at
                },
                info:{
                    id:val.details['C2_ID, MINI_DIARY_C2'],
                    mid:val.event?.MS,
                    bet365id:val.id,
                    name:`${val.home?.name} vs ${val.away?.name}`,
                    sport:val.sport,
                    league:val.details[0]?.COMPETITION_NAME,
                    start_time:toTime(val.details[0]?.TMR_UPDATED),//toTime(val.TMR_UPDATED),
                    start_date:toDate(val.details[0]?.TMR_UPDATED),
                    start_ts:val.details[0]?.TMR_UPDATED,
                    start_ts_utc:val.details[0]?.TMR_UPDATED,
                    period:val.details["EXTRA_DATA_2, TEAM_ODDS_A"],
                    score:`${homeTeam[0]['SCORE, SCORES_COLUMN']}:${awayTeam[0]['SCORE, SCORES_COLUMN']}`,
                    ball_pos: "",
                    state_info: null,
                    state: "",
                    minute: val.details,
                    seconds: `${val.details[0]?.TM}:${twoDigitSeconds(val.details[0]?.TS)}`
                },
            
            team_info:{
                home:{
                    name:homeTeam[0]?.NA,
                    score:homeTeam[0]?.SC,
                    color:"",
                    kit_color:homeTeam[0]?.KC,
                    Serve:""
                },
                away:{
                    name:awayTeam[0]?.NA,
                    score:awayTeam[0]?.SC,
                    kit_color:awayTeam[0]?.KC,
                    Serve:""
                }
            },
            bonus:{},
            stream:"",
            stats:transformStats(stats),
            extra:transformExtra(extra),
            history:{},
            //odds:transformOdds(val.odds)
        },
        }
    })

    return result
}


function toDate(dateString){

    if(dateString === undefined) return ""
    const year = dateString.slice(0, 4)
    const month = dateString.slice(4, 6)
    const day = dateString.slice(6, 8)

    return `${month}.${day}.${year}`
}

function toTime(dateString){

    if(dateString === undefined) return ""
    const hours = dateString.slice(8, 10)
    const minutes = dateString.slice(10, 12)

    return `${hours}:${minutes}`
}

function toTimeWithSeconds(dateString){
    if(dateString === undefined) return ""
    const hours = dateString.slice(8, 10)
    const minutes = dateString.slice(10, 12)
    const seconds = dateString.slice(12, 14)

    return `${hours}:${minutes}:${seconds}`
}

function UnixToDate(data){
    const newDate = new Date(data * 1000)
    const date = newDate.toLocaleDateString('en-US',{day:"numeric",month:'2-digit',year:'numeric'})
    const time = newDate.toLocaleTimeString('it-IT',{hour:'2-digit',minute:'2-digit',second:'2-digit'})

    const formattedDate = `${date.replace(/\//g, ".")} ${time}`

    return formattedDate
}

function determineSport(sportName){
    let sports=[
        {
            sportId: 1,
            sportName:'Soccer'
        },
        {
            sportId: 18,
            sportName:'Basketball'
        },
        {
            sportId: 91,
            sportName:'Volleyball'
        },
        {
            sportId: 17,
            sportName:'Hockey'
        },
        {
            sportId: 16,
            sportName:'Baseball'
        }
    ]

    const filtered = sports.filter((val)=> val.sportName === sportName)

    const found = filtered.length ? true : false
    const sportId = filtered.length ? filtered[0].sportId : null

    return {found:found,sportId:sportId}
}

function transformStats(stats){

    let scoresColumn = stats.filter(v => v.type === 'SC')
    let homeTeam = stats.filter(v => v.type === 'SL' && v.ID === '0')
    let awayTeam = stats.filter(v => v.type === 'SL' && v.ID === '1')

    let result = scoresColumn?.map(i => {

        if(!homeTeam[i.ID] || !awayTeam[i.ID]){
            
            return undefined

        } else{

            return{
                [i.ID]:{
                    
                    name:i.ID === "0" ? 'ITeam' : i.NA,
                    home:homeTeam[i.ID]?.hasOwnProperty('DATA_1') ? homeTeam[i.ID]?.DATA_1 : "0",
                    away:awayTeam[i.ID]?.hasOwnProperty('DATA_1')? awayTeam[i.ID]?.DATA_1 : "0"
                }
            }
            
        }
       
    })

    let obj = {}

    result = result?.filter((val)=> val)

    result.map((val,i)=>
        obj[i] = val[i]
    )

    return obj

}

function transformExtra(extra){

    extra.sort((a, b)=>
        a.ID - b.ID
    )

   let result = extra?.map((i,idx)=>{
        return{
            [idx]:{
                code:i.ID,
                minute:i.TM,
                value:i.LA
            }
        }
   })

    let obj = {}

    result.map((val,i)=>
        obj[i] = val[i]
    )

    return obj
}

function transformOdds(odds){
    if(!odds) return {}

    let keys = Object.keys(odds).filter((val) => val && !['FI','event_id','schedule'].includes(val))

    let sp = {}
    keys.forEach((val)=>{
        if(val === undefined) return
        
        let spVal = odds[val].sp
            
        for(const spKey in spVal){
            
            if((odds[val].sp).hasOwnProperty(spKey)){
                let spValInsideVal = spVal[spKey]
               
                spValInsideVal['short_name'] =  spVal[spKey].name
                spValInsideVal['suspend'] =  "0"
                spValInsideVal['order'] =  ""
                spValInsideVal['participants'] = spValInsideVal.odds === undefined ? {} : transformParticipants(spValInsideVal.odds)  

                delete spValInsideVal['odds']
                
                sp[spVal[spKey].id] = spVal[spKey] 
            }
        }
        
    })

    return sp
}

function transformParticipants(participants){
    let obj ={}

    participants = participants.map((val)=>{
        obj[val.id] = {}
        let key = obj[val.id]

        key['id'] = val.id
        key['name'] = val.name
        key['short_name'] = val.name
        key['value_eu'] = val.odds
        if(val.hasOwnProperty('handicap')) key['handicap'] = val.handicap
        key['suspend'] = "0"
    })
    return obj
}

function twoDigitSeconds(secs){

    if(secs === undefined) return ""
    return secs.length === 1 ? `${0}${secs}` : secs
}



module.exports = { transform }