var cheerio = require('cheerio')
var axios = require('axios')

var Service = require('../services/task2')
var helper = require('../helper/helper.js')
var url = require('../constants/url')

module.exports = {
	scrape: async ctx => {

		const res = await axios.get(url.goPickLadder)
		const $ = cheerio.load(res.data)

		var service = new Service($)
			
		ctx.body = helper.prettify(service.transformToObject())
	}
}