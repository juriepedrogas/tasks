const axios = require('axios')
const url = require('../constants/url')
const helper = require("../helper/helper.js")
const service = require("../services/task3.js")
const service1 = require("../services/task1.js")
const task3data = require('../data/task3.json')
require('dotenv').config()

async function transform(ctx){
	console.log('task3 fetched')
	//Determine Sports
	if(!service.determineSport(ctx.params.sportName).found) ctx.throw(404,'Not Found')

	//Fetch inPlay Filter Results
	const inPlayFilterResults = await axios.get(`${url.inPlayFilter}&sport_id=${service.determineSport(ctx.params.sportName).sportId}`)
	let inPlayFilter = inPlayFilterResults.data.results

	//Fetch inPlay
	const inPlayResults = await axios.get(url.inPlay)
	let inPlay = await service1.transform(inPlayResults?.data.results[0])
	
	//Getting Sport Name via inPlay Results
	const sport = inPlay?.find(ival => 
		ival.type === 'CLASSIFICATION' && 
		ival.ID === service.determineSport(ctx.params.sportName).sportId.toString()
	)

	let inPlayFilterWithCompleteDetails = await Promise.all(inPlayFilter.map(async val => {

		//getting state and ball.pos in inPlayEvent
		let inPlayEventDetailsState = await axios.get(`${url.inPlayEvent}&FI=${val.id}&stats=1`) 
		inPlayEventDetailsState = inPlayEventDetailsState.data?.results ? inPlayEventDetailsState.data?.results[0]
		.find(is => is.type === 'EV') : {}

		//Fetching inPlayEvent
		const inPlayEventDetailsOdds = await axios.get(`${url.inPlayEvent}&FI=${val.id}`) 
		
		let inPlayEventDetails = inPlayEventDetailsOdds.data.results ? 
			await service1.transform(inPlayEventDetailsOdds.data?.results[0]) : []
		
		let filteredInPlay = inPlay?.find(fval => fval.type === 'EVENT' && fval.ID.includes(val.r_id))

		//Passing all the data
		return{
			...val,
			sport: sport?.NAME,
			events: {stats:inPlayResults?.data.stats, results:await filteredInPlay},
			details:inPlayEventDetails,
			state: inPlayEventDetailsState?.VC ?? "" ,
			pos: inPlayEventDetailsState?.XY ?? ""

		}

	}))

	// let inPlayFilterWithCompleteDetails = task3data

	let results = service
	.restructureJSON(
		inPlayFilterWithCompleteDetails,
		service.determineSport(ctx.params.sportName).
		sportId
	)
	
	let obj = {}

	//Transforming Array to JSON
	results.forEach(val => {
		let objKey = Object.keys(val)[0]
		obj[objKey] = val[objKey]
	})

	const coreUpdated = inPlayResults?.data.stats.update_dt.replace(/-/g, ".").split(" ")

	let result = {
		bm:"bet365",
		updated:`${trasnformUpdatedTime(coreUpdated[0])} ${coreUpdated[1]}`,
		updated_ts:inPlayResults?.data.stats.update_at,
		events:obj
	}

	ctx.body = helper.prettify(result)

}

module.exports = { transform }

function trasnformUpdatedTime(data){
	let arr = data.split(".")

	return `${arr[1]}.${arr[2]}.${arr[0]}`
}