
const service = require('../services/task1.js')
const helper = require('../helper/helper.js')
const url = require('../constants/url.js')
const axios = require('axios')

require('dotenv').config()

async function inPlay(ctx){

	let result = await axios.get(url.inPlay)
	
	result = result.data?.results[0]
	ctx.body = helper.prettify({results:[await service.transform(result)]})

}

async function inPlayFilter(ctx){

	let result = await axios.get(`${url.inPlayFilter}&sport_id=${ctx.params.sport_ID}`)

	result = result?.data.results
	ctx.body = helper.prettify({results:[await service.transform(result)]})
}

async function inPlayEvent(ctx){

	let result = await axios.get(`${url.inPlayEvent}&FI=${ctx.params.FI}`)
	
	if(result.data?.error){
		return ctx.body = result.data
	}
	
	result = result.data?.results[0]
	ctx.body = helper.prettify({results:[await service.transform(result)]})

}

module.exports = {
		inPlay,
		inPlayFilter,
		inPlayEvent
}