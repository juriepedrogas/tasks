require('dotenv').config()

module.exports={
	//task2
	goPickLadder : `${process.env.GOPICKLADDER}`,

	//task3
	inPlay :        `${process.env.INPLAY}${process.env.TOKEN}`,
	inPlayEvent :   `${process.env.INPLAYEVENT}${process.env.TOKEN}`,
	inPlayFilter :  `${process.env.INPLAYFILTER}${process.env.TOKEN}`,
	
}