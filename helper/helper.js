const axios = require('axios')
require('dotenv').config()

function prettify(data){
  return JSON.stringify(data,null,2)
}

function transformToObj(arr){
  let obj = {}

	arr.forEach((val,i) =>
		obj[i] = val[i]
	)

	return obj
}

module.exports = {
	prettify,
	transformToObj
}