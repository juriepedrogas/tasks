var Router = require('koa-tree-router');
var router = new Router();

// Controllers
var task1Controller = require('../controllers/task1Controller.js')
var task2Controller = require('../controllers/task2Controller.js')
var task3Controller = require('../controllers/task3Controller.js')
var testController = require('../controllers/testController.js')
var parallelController = require('../controllers/parallelController.js')

// Endpoints
const task1 = router.newGroup('/task1')
task1.get('/inPlay',task1Controller.inPlay)
task1.get('/inPlayFilter/:sport_ID',task1Controller.inPlayFilter)
task1.get('/inPlayEvent/:FI', task1Controller.inPlayEvent)
  

router.get('/task2', task2Controller.scrape)

router.get('/task3/:sportName', task3Controller.transform)

router.get('/test/:sportName', testController.transform)

router.get('/parallel/:sportName', parallelController.transform)

module.exports = router;