var knex = require('../config/db')

async function transform(result){
    
	const ref = await knex('fields')
		.select('*')
		.then(res => {
				return res
		})

	const data = result

	// Helper function
	function match(val){
		let result = ref.filter(v => { return v.field === val})[0]
		return result ? result.description : val
	}
	
	let results = data?.map(i => {
		const obj = i

		//Looping thru object
		let newObj = {}
		for(const key in obj){
			//Reading keys
			if(key === 'type'){
				newObj['type'] = match(obj[key])
			}else{
				newObj[match(key)] = obj[key]
			}
		}

		return newObj
	})

	return results

}

module.exports = {
    transform
}