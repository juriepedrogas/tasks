const transformOdds = require('./task3Odds/TransformOdds.js')
const helper = require('../helper/helper.js')

let lastStateVal = ""

function restructureJSON(data, sport){
	const result = data.map(val => {
			
		const homeTeam = val.details.find(v => v.type === 'TEAM' && v.ORDER === '0')
		const awayTeam = val.details.find(v => v.type === 'TEAM' && v.ORDER === '1')
		const stats = val.details.filter(v => ["SCORE, SCORES_COLUMN", "SCORES_CELL"].includes(v.type))
		const extra = val.details.filter(v => v.type === 'INFO_POD_DETAIL_1, STAT, POD_BODY_TEXT_1, STAKE' && v['LABEL, INFO_POD_LINK_1_ID'])
		const minutes = val.events.results?.['STAT_TIME, TMR_MINS'] ?? ""
		const seconds = val.events.results?.["TMR_SECS, TOTE_NAMES"] ?? ""
		const Teams = [
			{
				NAME:homeTeam?.NAME, 
				TYPE:'Home'
			}, 
			{
				NAME:awayTeam?.NAME, 
				TYPE:'Away'
			}
		]
		const eventId =  val.events.results?.["C2_ID, MINI_DIARY_C2"]
		const period = val.events.results?.["EXTRA_DATA_2, TEAM_ODDS_A"] ? 
			val.events.results?.["EXTRA_DATA_2, TEAM_ODDS_A"] : 
			val.events.results?.["CLOSE_BETS_PRESENTATION_PULL_DISABLED, CURRENT_PROGRESS, CURRENT_PERIOD"] ?
			val.events.results?.["CLOSE_BETS_PRESENTATION_PULL_DISABLED, CURRENT_PROGRESS, CURRENT_PERIOD"] :
			""
		const coreUpdatedVal = val.events.results?.["TMR_UPDATED"]
		return{
			[eventId]:{
				core: {
					safe: "",
					removed: "",
					stopped: "0",
					blocked: "0",
					finished: "0",
					nc: "0",
					updated: transformcoreUpdated(coreUpdatedVal).date,
					updated_ts: transformcoreUpdated(coreUpdatedVal).ts
				},
				
				info: {
					id: eventId,
					mid: val.event?.MS,
					bet365id: val.id,
					name: `${val.home?.name} vs ${val.away?.name}`,
					sport: val.sport,
					league: val.details[0]?.COMPETITION_NAME,
					start_time: UnixToDate(val.time).split(" ")[1].split(":").splice(0,2).join(':'),
					start_date: UnixToDate(val.time).split(" ")[0],
					start_ts: val.time,
					start_ts_utc: val.time,
					period: transformPeriod(sport, period),
					score: val.events.results?.["SHORT_SCORE, SUSPENDED_SELECTION"] ?? "",
					ball_pos: val?.pos,
					state_info: null,
					state: getLastState(val.state?.includes('^') ? val.state?.split('^')[0] : val?.state),
					minute: twoDigits(seconds === "00" ? 0 : `${parseInt(minutes)}`) ,
					seconds: `${twoDigits(minutes)}:${twoDigits(seconds)}`
				},
		
				team_info:{
					home: {
						name: homeTeam?.NAME,
						score: homeTeam?.['SCORE, SCORES_COLUMN'] ?? "",
						color: homeTeam?.KIT_COLORS.split(',')[0] ?? "",
						kit_color: homeTeam?.KIT_COLORS ?? "",
						Serve: ""
					},
					away: {
						name: awayTeam?.NAME ?? "",
						score: awayTeam?.['SCORE, SCORES_COLUMN'] ?? "",
						color: awayTeam?.KIT_COLORS.split(',')[0] ?? "",
						kit_color: awayTeam?.KIT_COLORS ?? "",
						Serve: ""
					}
				},
				bonus:{},
				stream: "",
				stats: transformStats(stats),
				extra: transformExtra(extra),
				history: {},
				odds: transformOdds(val.details, Teams, eventId, sport)
			},
		}
	})

	return result
}

function UnixToDate(data){

	const newDate = new Date(data * 1000)
	const date = newDate.toLocaleDateString('en-US',{day:"numeric", month:'2-digit', year:'numeric'})
	const time = newDate.toLocaleTimeString('it-IT',{hour:'2-digit', minute:'2-digit', second:'2-digit'})

	const formattedDate = `${date.replace(/\//g, ".")} ${time}`

	return formattedDate
}

function determineSport(sportName){

	let sports= [
		{
			sportId: 1,
			sportName:'Soccer'
		},
		{
			sportId: 18,
			sportName:'Basketball'
		},
		{
			sportId: 91,
			sportName:'Volleyball'
		},
		{
			sportId: 17,
			sportName:'Hockey'
		},
		{
			sportId: 16,
			sportName:'Baseball'
		}
	]

	const filtered = sports.filter(val => val.sportName === sportName)

	const found = filtered.length ? true : false
	const sportId = filtered.length ? filtered[0].sportId : null

	return {
		found:found, 
		sportId:sportId
	}
}

function transformStats(stats){

	let scoresColumn = stats.filter(v => v.type === 'SCORE, SCORES_COLUMN')
	let homeTeam = stats.filter(v => v.type === 'SCORES_CELL' && v.ID === '0')
	let awayTeam = stats.filter(v => v.type === 'SCORES_CELL' && v.ID === '1')

	let result = scoresColumn?.map(i => {
		return{
			[i.ID]:{
				name:i.ID === "0" ? 'ITeam' : i.NAME,
				home:homeTeam[i.ID]?.hasOwnProperty('DATA_1') ? homeTeam[i.ID]?.DATA_1 : "0",
				away:awayTeam[i.ID]?.hasOwnProperty('DATA_1') ? awayTeam[i.ID]?.DATA_1 : "0"
			}
		}
	})

	return helper.transformToObj(result)
}

function transformExtra(extra){

	extra.sort((a, b) =>
		a.ID - b.ID
	)

	let result = extra?.map((i, idx) => {
		return{
			[idx]:{
				code:i.ID,
				minute:i['STAT_TIME, TMR_MINS'],
				value:i['LABEL, INFO_POD_LINK_1_ID']
			}
		}
	})

	return helper.transformToObj(result)
}

function twoDigits(secs){

	if(!secs){ 
		return ""
	}

	return secs.length === 1 ? `${0}${secs}` : secs
}

function getLastState(state){

	if(state !== "") {
		lastStateVal = state
	}

	return state === "" ? lastStateVal : state 
}

function transformcoreUpdated(data){

	if(!data) {
		return ""
	}

	let results = {}

	let d = data.slice(0, 8).split("")
	let t = data.slice(8).split("")

	let dr = `${d[4]}${d[5]}.${d[6]}${d[7]}.${d[0]}${d[1]}${d[2]}${d[3]} ${t[0]}${t[1]}:${t[2]}${t[3]}:${t[4]}${t[5]}`

	results ={
		...results,
		date: `${d[6]}${d[7]}.${d[4]}${d[5]}.${d[0]}${d[1]}${d[2]}${d[3]} ${t[0]}${t[1]}:${t[2]}${t[3]}:${t[4]}${t[5]}`,
		ts:new Date(dr).valueOf() / 1000
	}

	return results
	
}


function transformPeriod(sport, data){

	console.log(sport, data)
	switch(sport){
		case 1:
		break

		case 18:
			const pr = new Intl.PluralRules("en-US", { type: "ordinal" });

		const suffixes = new Map([
			["one", "st"],
			["two", "nd"],
			["few", "rd"],
			["other", "th"],
		]);
		const formatOrdinals = (n) => {
			const rule = pr.select(n);
			const suffix = suffixes.get(rule);
			return `${n}${suffix}`;
		};

		return data ? `${formatOrdinals(data.split("")[1])} Quarter` : ""
		case 16:

		break
	}

}

module.exports = {
    UnixToDate,
    determineSport,
    restructureJSON,
}



