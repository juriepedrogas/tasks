
module.exports = class Helper{
	constructor($){
			this.$ = $
	}

	transformToObject(){
		const rounds = this.$('div.record-panel').map((i, element) => {
				
			const panelHead = this.$(element).find('.panel-head').text()
			let obj = {}

			obj['date'] = this.transformToDate(panelHead)
			obj['round'] = panelHead.replace(/ *\([^)]*\)()|[ㄱ-ㅎㅏ-ㅣ가-힣]/g, "")

			this.$(element).find('ul li span').map((s, span) => {
				obj[`item${s + 1}`] = this.$(span).attr('class') 
			})

			return obj
		})
		.get()
		.filter(Boolean)
		
		return rounds
	}

	transformToDate(string){

		const sanitizedStr = string.match(/\(([^)]+)\)/)[1]

		return sanitizedStr.substring(0,8)
		
	}

}