module.exports = (market) => {
	let marketArr = Object.values(market)
	let teamName = []
	let participantsWithTeamName = {}
	
	marketArr.forEach((mval, mi) => {
		let participantsArr = Object.values(mval.participants)
		
		if(mi === 0){
				
			participantsArr.forEach(pval => {
				teamName.push({TEAM:pval.NAME, ORDER:pval.ORDER})
			})

		}else{
			
			participantsArr.forEach(pval => {
				let participantsVal = pval
				
				participantsWithTeamName = {
					...participantsWithTeamName,
					[pval.ID]:{
						...pval,
						NAME:`${teamName.find(hval => hval.ORDER === pval.ORDER)?.TEAM}/${mval.NAME}`
					}
				}
			})
				
		}
	})

	return participantsWithTeamName
}