const transform = require('../transform')

module.exports = (market,val,id) => {
    
	let objToPush = {}

	objToPush = {
		...val,
		[val.ID]:{
			id:val.ID,
			name:val.NAME,
			short_name:val.NAME,
			suspend:val["SUCCESS, SUSPENDED"],
			order:val.ORDER,
			participants:transform.participants(market,id,val.ID)
		}
	}

	//Looping and replacing a new ID in Participants
	objToPush[val.ID].participants = transform.newIdInParticipants(objToPush[val.ID],val.ID,id)
	

	return objToPush

}