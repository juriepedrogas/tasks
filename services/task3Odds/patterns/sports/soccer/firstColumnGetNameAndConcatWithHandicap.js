module.exports = (market) => {
  
	let marketKeys = Object.keys(market)
	let overUnder = []
	let participantsWithhandicap = {}
	
	marketKeys.forEach((mval, mi) => {
		let participantsKey = Object.keys(market[mval].participants)
		
		if(mi === 0){
				
			participantsKey.forEach(pval => {
				let participantsVal = market[mval].participants[pval]
				
				overUnder.push({NAME:participantsVal.NAME, ORDER:participantsVal.ORDER})
			})

		}else{
			
			participantsKey.forEach(pval => {
				let participantsVal = market[mval].participants[pval]
				
				participantsWithhandicap = {
					...participantsWithhandicap,
					[participantsVal.ID]:{
						...participantsVal,
						NAME:`${overUnder.find(hval => hval.ORDER === participantsVal.ORDER)?.NAME}/${market[mval]?.NAME} ${participantsVal?.HANDICAP}`
					}
				}
			})
				
		}
	})
	return participantsWithhandicap
}