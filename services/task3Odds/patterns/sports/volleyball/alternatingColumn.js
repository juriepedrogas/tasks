module.exports = (market) => {
  
  let marketOrder = {}
  

  const marketKeys = Object.keys(market)
  //looping markets
  marketKeys.forEach((mval, mi) => {
    if(mi === 0){ 

      const participants = market[mval].participants
      const participantKeys = Object.keys(participants)

      //looping participants 
      participantKeys.forEach((pval, pi) => {
        //Since it alternating with two teams will loop twice
        for(let i =0; i < 2; i++){
        
          //Check if the val contains alphabet exclude in alternate
          const regex = /[a-zA-Z]/g
          let pName = participants[pval].NAME
          const originalName = participants[pval].NAME
          if(regex.test(participants[pval].NAME)){

            //Adding T1 & T2 in name
            participants[pval].NAME = !i ? `T1 ${pName}` : `T2 ${pName}`
            participants[pval].ID = `${pi}${i}`
            marketOrder = {
              ...marketOrder,
              [`${pi}${i}`]:{
                ...participants[pval]
              }
            }
            //Back to Original Name inorder to not overlap the name
            participants[pval].NAME = originalName

          }else{

            //Alternate Score
            const splitName = pName.split('-')
            participants[pval].NAME = !i ? pName : `${splitName[1]}-${splitName[0]}`
            participants[pval].ID = `${pi}${i}`
            marketOrder = {
              ...marketOrder,
              [`${pi}${i}`]:{
                ...participants[pval]
              }
            }
            //Back to Original Name inorder to not overlap the name
            participants[pval].NAME = originalName

          }

        }
      })
    }else{
      //Tiwasa ni
      const marketOrderKeys = Object.keys(marketOrder)
      let odd = []
      let even = []
      const participantsVal = Object.values(market[mval].participants)

      //Separating by Team
      marketOrderKeys.forEach(moval => { 
        moval % 2 ?  odd.push(marketOrder[moval]) : even.push(marketOrder[moval]) 
      })

      // Putting Odds in marketOrder
      if(market[mval].NAME === 'Home'){
        even.forEach((eval, ei) => {
          eval.ODDS = participantsVal[ei]?.ODDS
          eval["SUCCESS, SUSPENDED"] = participantsVal[ei]["SUCCESS, SUSPENDED"]
        })
      }else{
        odd.forEach((oval, oi) => {
          odd.ODDS = participantsVal[oi]?.ODDS
          oval["SUCCESS, SUSPENDED"] = participantsVal[oi]["SUCCESS, SUSPENDED"]
        })
      }
      // Concat 2 arr and sort
      even.concat(odd).forEach(val => {
        marketOrder = {
          ...marketOrder,
          [val.ID]:{
            ...val
          }
        }
      })

    }
    
  })

  return marketOrder

}