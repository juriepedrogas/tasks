module.exports = (market,mgName) => {
    
	const marketArr = Object.values(market)
	const indexToSplit = marketArr.map(val => {return val.NAME}).indexOf(" ", 1) 

	let result = {}

	let twoWay = marketArr.slice(0,indexToSplit)
	let threeWay = marketArr.slice(indexToSplit)

	let twoWayOdds = {}
	let threeWayOdds = {}

	twoWay.forEach((oval, i) => {
		let participants = oval.participants
		let participantsKey = Object.keys(participants)

		if(i === 0){

			participantsKey.forEach( pval => {
				participants[pval] = {
					...participants[pval], 
					NAME:`${mgName} ${participants[pval]?.NAME} `
					}
			})

			twoWayOdds = {...oval.participants}

		}else{
				
			let twoWayOddsKey = Object.keys(twoWayOdds)

			twoWayOddsKey.forEach(okval => {
					
				participantsKey
				.filter(pval => !participants[pval].NAME?.includes('Total'))
				.forEach(pval => {
					if( participants[pval].ORDER === twoWayOdds[okval].ORDER){

						participants[pval].NAME = oval.NAME

						twoWayOdds[okval].participants = {
							...twoWayOdds[okval].participants,
							[participants[pval].ID]:{
								...participants[pval]
							}
						}
							
					}
				})
			})
		
		}

	})
	
	threeWay.forEach((oval, i) => {
		let participants = oval.participants
		let participantsKey = Object.keys(participants)

		if(i === 0){

			participantsKey.forEach(pval => {
				participants[pval] = {
					...participants[pval], 
					NAME:`${mgName} ${participants[pval]?.NAME}`
					}
			})

			threeWayOdds = {...oval.participants}

		}else{
				
			let threeWayOddsKey = Object.keys(threeWayOdds)

			threeWayOddsKey.forEach(okval=>{
				participantsKey
				.filter(pval => !participants[pval].NAME?.includes('Total'))
				.forEach(pval => {
						
					if(participants[pval].ORDER === threeWayOdds[okval].ORDER){

						participants[pval].NAME = oval.NAME

						threeWayOdds[okval].participants = {
							...threeWayOdds[okval].participants,
							[participants[pval].ID]:{
								...participants[pval]
							}
						}
					}
				})
			})
		}

	})

	result = {
		...result, 
		...twoWayOdds,
		...threeWayOdds
	}

	return result

}