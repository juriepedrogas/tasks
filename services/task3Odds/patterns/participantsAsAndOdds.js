module.exports = (market, val) => {

	let marketArr = Object.values(market)

	let odds = {}

	marketArr.forEach((mval, i) => { 
		if(i === 0){

			odds = {...odds ,...mval.participants}

		}else{

			let participantsArr = Object.values(mval.participants)
			let oddsKey = Object.keys(odds)
			
			//Loop the new Odds
			oddsKey.forEach(oval => { 

				odds[oval]['marketName'] = val.NAME
				
				participantsArr.forEach(pval => {

					if(odds[oval].ORDER === pval.ORDER){

						pval.NAME = mval.NAME
						odds[oval]['participants'] = {
								...odds[oval]['participants'],
								[pval.ID]:{...pval}
						}

					}
				})
			})

		}
	})

	return odds
}