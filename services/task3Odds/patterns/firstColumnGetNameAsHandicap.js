module.exports =  (market) => {
	let marketArr = Object.values(market)
	let handicap = []
	let participantsWithHandicap = {}

	marketArr.forEach((mval, i)=>{
		if(i === 0){

			let handicapArr = Object.values(mval.participants)

			handicapArr.forEach(hval => {
				handicap.push({ORDER:hval.ORDER, HANDICAPVAL:hval.NAME})
			})

		}else{

			let participantsArr = Object.values(mval.participants)
			
			participantsArr.forEach(pval => {
				
				participantsWithHandicap = {
					...participantsWithHandicap,
					[pval.ID]:{
						...pval,
						NAME:mval.NAME,
						ORDER:pval.ORDER,
						HANDICAP:handicap.find(hval => hval.ORDER === pval.ORDER)?.HANDICAPVAL
					} 
				}
			})

		}
	})

	return participantsWithHandicap
}