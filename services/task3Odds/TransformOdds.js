
const participantsAsAndOdds = require('./patterns/participantsAsAndOdds')
const firstColGetNameAsHandicap = require('./patterns/firstColumnGetNameAsHandicap')
const firstColGetNameAndConcat = require('./patterns/firstColumnGetNameAndConcat')
const twoColsParticipantsAsAnOdds = require('./patterns/twoColumnsParticipantsAsAnOdds')
const alternatingColumn = require('./patterns/sports/volleyball/alternatingColumn')
const firstColGetNameAndConcatWithHandicap = require('./patterns/sports/soccer/firstColumnGetNameAndConcatWithHandicap')

let id = ""

module.exports = function transformOdds(data, team, idx, sport){
    
	id = idx

	data = data.filter(val => ["MARKET_GROUP", "MARKET", "PARTICIPANT"].includes(val.type))

	// Replace Team Name to Home and Away
	data = data.map(val => {
		return {
			...val,
			NAME:val.NAME
			?.replace(team[0].NAME, team[0].TYPE)
			.replace(team[1].NAME, team[1].TYPE)
		}
	})

	//Reversing data to make the participant as first before the market and marketGroup
	// to isolate the participant by market
	data.reverse()
	return Odds(data,sport)
  
}

function Odds(data,sport){

	let participants = {}
	let market = {}
	let marketGroup = {}

	data.forEach(val => {
		switch(val.type){

			case 'PARTICIPANT':
					
				participants = {
					...participants,
					[val.ID ? val.ID : val.TOPIC_ID ]:{...val} 
				}

			break

			case 'MARKET':
					
				market = 
				{
					...market, 
					[val.ORDER]:{
						...val,
						participants:{...participants}
					}
				}
				// Clean participants object for the next market
				participants = {}

			break

			case 'MARKET_GROUP':

				//All Sports
				//will transform the first array market->participants as an individual odds or 2 columns 
				if(
						
					(sport === 17 && 
						val.NAME.includes('Lines') && 
						val.NAME !== "Game Lines" && 
						val.NAME !== "Draw No Bet" && 
						val.NAME !== "Alternative Puck Lines" 
					) ||
					(sport === 16 || 
						sport === 18 && 
						[
							"Game Lines",
							"Innings Lines", 
							"Half Lines", 
							"Quarter Lines"
						].some(a => val.NAME?.includes(a))
					) ||
					(sport === 91 && val.NAME.includes('Lines')) ||
					(sport !== 18 && val.NAME === 'Odd/Even')
						
				){
					const isOneCol = !['Half Lines','Quarter Lines'].some(str => val.NAME.includes(str))
					
					let odds = isOneCol ? participantsAsAndOdds(market,val) : twoColsParticipantsAsAnOdds(market,val.NAME)

					let oddsArr = Object.values(odds)

					oddsArr.forEach(o => {
						
						let params = {
							mg: marketGroup,
							id: o.ID,
							name: isOneCol ? `${o.marketName} ${o.NAME}` : o.NAME,
							suspend: val["SUCCESS, SUSPENDED"],
							order: o.ORDER,
							participants: transformParticipants({0:o})
						}
						
						marketGroup = transformMarketGroup(params)
						
						//Looping and replacing a new ID in Participants
						marketGroup[o.ID].participants = newIdInParticipants(marketGroup[o.ID], o.ID)

					})
					
					// Clean market object for the next market group
					market = {}                    
				}
				// Not Done because the first column in market->participants has no id it will just show 1 odd instead of how many odds in the first col

				//All Sports
				// Will get the first array in market->participant as the name of the team and concat the name of participant
				else if (
					[
						'Result / Both Teams To Score',
						'Match Result and Total'
					].some((str => val.NAME?.includes(str)))
				){
					
					let participantsWithTeamName = firstColGetNameAndConcat(market)

					let params = {
						mg: marketGroup,
						id: val.ID,
						name: val.NAME,
						suspend: val["SUCCESS, SUSPENDED"],
						order: val.ORDER,
						participants: transformParticipantsIfNoMarket(participantsWithTeamName)
					}
					
					marketGroup = transformMarketGroup(params)

					//Looping and replacing a new ID in Participants
					marketGroup[val.ID].participants = newIdInParticipants(marketGroup[val.ID], val.ID)

					// Clean market object for the next market group
					market = {}

				}

				//VolleyBall
				else if(sport === 91 && ["Correct Score", "Score After"].some(a => val.NAME.includes(a))){
				
					let alternatedParticipants = alternatingColumn(market)

					let params = {
						mg: marketGroup,
						id: val.ID,
						name: val.NAME,
						suspend: val["SUCCESS, SUSPENDED"],
						order: val.ORDER,
						participants: transformParticipantsIfNoMarket(alternatedParticipants)
					}

					marketGroup = transformMarketGroup(params)

					//Looping and replacing a new ID in Participants
					marketGroup[val.ID].participants = newIdInParticipants(marketGroup[val.ID], val.ID)

					// Clean market object for the next market group
					market = {}
					
				}
				//Soccer
				else if(sport === 1 && 
					[
						"Next 10 Minutes", 
						"Team Clean Sheet", 
						"Asian Handicap"
					].some(a => val.NAME.includes(a))
				){

					if(val.NAME.includes("Next 10 Minutes")){
						
						let nameWithHandicap = firstColGetNameAndConcatWithHandicap(market)

						let params = {
							mg: marketGroup,
							id: val.ID,
							name: val.NAME,
							suspend: val["SUCCESS, SUSPENDED"],
							order: val.ORDER,
							participants: transformParticipantsIfNoMarket(nameWithHandicap)
						}
						
						marketGroup = transformMarketGroup(params)

						//Looping and replacing a new ID in Participants
						marketGroup[val.ID].participants = newIdInParticipants(marketGroup[val.ID], val.ID)

						// Clean market object for the next market group
						market = {}

					}

					else if(val.NAME === "Team Clean Sheet"){
						//let Odds = marketAsAnOdds(market)

						let marketArr = Object.values(market)

						//Handicap_Formatted as name
						marketArr.forEach(mval => {
							let participants = mval.participants
							let participantsArr = Object.values(participants)

							participantsArr.forEach(pval => {
								pval.NAME = pval["HANDICAP_FORMATTED"]
							})

							let params = {
								mg: marketGroup,
								id: mval.ID,
								name: `${mval.NAME} ${val.NAME}`,
								suspend: val["SUCCESS, SUSPENDED"],
								order: val.ORDER,
								participants: transformParticipantsIfNoMarket(mval.participants)
							}
							
							marketGroup = transformMarketGroup(params)

							//Looping and replacing a new ID in Participants
							marketGroup[mval.ID].participants = newIdInParticipants(marketGroup[mval.ID], val.ID)
							
						})

						// Clean market object for the next market group
						market = {}
					}

					else if(val.NAME.includes("Asian Handicap")){
						
						let result = []

						for(const mk in market){
							const participants = market[mk].participants
							for(const pk in participants){
								participants[pk].NAME = market[mk].NAME
								result.push(participants[pk])
								
							}
						}
						result = Object.assign({}, result.sort((a, b) => a.ORDER - b.ORDER))
						
						let params = {
							mg: marketGroup,
							id: val.ID,
							name: val.NAME,
							suspend: val["SUCCESS, SUSPENDED"],
							participants: transformParticipantsIfNoMarket(result)
						}

						marketGroup = transformMarketGroup(params)
	
						//Looping and replacing a new ID in Participants
						marketGroup[val.ID].participants = newIdInParticipants(marketGroup[val.ID], val.ID)
	
						// Clean market object for the next market group
						market = {}
					}

				}

				//Basketball and Volleyball
				else if((sport === 18 || sport === 91) && 
					[
						"Quarter Home To Score", 
						"Quarter Away To Score",
						"Quarter Both Teams To Score",
						"Lead After",
						"Race to"
					].some(a => val.NAME?.includes(a))
				){

					if([
						"Quarter Home To Score", 
						"Quarter Away To Score",
						"Quarter Both Teams To Score",
						"Lead After",
						"Race to"
					].some(a => val.NAME.includes(a))){

						let participantsWithHandicap = firstColGetNameAsHandicap(market)

						//Alternate yes or no
						let arr = Object.values(participantsWithHandicap).sort((a, b) => a.ORDER - b.ORDER)
						let alternatedParticipants = Object.assign({}, arr)
						let result = Object.values(transformParticipantsIfNoMarket(alternatedParticipants))
						
						let params = {
							mg: marketGroup,
							id: val.ID,
							name: val.NAME,
							suspend: val["SUCCESS, SUSPENDED"],
							order: val.ORDER,
							participants: Object.assign({}, result)
						}
						
						marketGroup = transformMarketGroup(params)

						//Looping and replacing a new ID in Participants
						marketGroup[val.ID].participants = newIdInParticipants(marketGroup[val.ID], val.ID)

						// Clean market object for the next market group
						market = {}
						
					}
						
				}
				
				//Exclude if the ID or format is not found
				// Legend: A - Any of the string, S - Case Sensitive in string 
				// A. Team Totals/All Sports -  No id in Market
				// A. Winning Margin - No id in Participant
				// A. Point Betting/Volleyball - Name in Bet365 and inPlay goalserve are not the same
				// A. Half Time/Full/Volleyball Time - not Found format in inPlay goalserve but transformable
				// A. Exact Goals/Soccer - not Found format in inPlay goalserve but transformable
				// S. Corners Race/Soccer - not Found format in inPlay goalserve but transformable
				// S. Race/Soccer - not Found format in inPlay goalserve but transformable
				// S. Corners/Soccer - not Found format in inPlay goalserve but transformable
				// S. 2-Way Corners/Soccer - not Found format in inPlay goalserve but transformable
				// S. 1st Half Asian Corners/Soccer - not Found format in inPlay goalserve and not transformable
				// S. Time of 1st Goal/Soccer - not Found format in inPlay goalserve and not transformable
				// S. Alternative Match Goals/Soccer - not Found format in inPlay goalserve and not transformable
				// S. Team to Score in 2nd Half/Soccer - not Found format in inPlay goalserve and not transformable
				// S. First Half Goals/Soccer - not Found format in inPlay goalserve but transformable
				// A. Goal Line/Soccer - not Found format in inPlay goalserve but transformable
				// S. Match Time Result/Soccer - not Found format in inPlay goalserve not sure if transformable
				// A. Time of/Soccer - not Found format in inPlay goalserve and not transformable
				// A. Odd/Even/Basketball - no Id in Participant
				// A. Double Result/Basketball - not Found format in inPlay goalserve and not transformable
				// S. 1st Half/Basketball - weird format too many participants
				// A. Race to/Basketball - weird format too many participants
				// A. Winning Margin/Basketball - no Id in Market
				// A. Winning Margin/Baseball - no Id in Market
				// S. Match Correct Score/Baseball - no Id in Market
				// S. Alternative Money Line and Total/Baseball - not Found format in inPlay goalserve and not transformable
				// S. Half Time/Full Time/Soccer - not Found format in inPlay goalserve and not transformable
				// S. Game Lines/ Hockey - found format but the bet365 format is weird there is empty name participant and some has no draw participant
				// A. Correct Score/ Hockey - Not Found format and not transformable
				// A. Both teams to score at least/ Hockey - no Id in Market
				// S. Draw No Bet/ Hockey - no Id in Market
				// S. Alternative Puck Lines - No format found and not transformable
				else if(
					(
						//Volleyball
						(sport === 91 &&
							[
								"Winning Margin", 
								"Point Betting", 
								"Half Time/Full Time", 
							].some(a => val.NAME.includes(a))
						) ||
						//Soccer
						(sport === 1 &&
							[
								"Exact Goals",
								"Goal Line",
								"Time of",
							].some(a => val.NAME.includes(a)) ||
							val.NAME === "Corners Race" || 
							val.NAME === "Race" ||
							val.NAME === "Corners" ||
							val.NAME === "2-Way Corners" ||
							val.NAME === "1st Half Asian Corners" ||
							val.NAME === "Time of 1st Goal" || 
							val.NAME === "Alternative Match Goals" ||
							val.NAME === "Team to Score in 2nd Half" || 
							val.NAME === "First Half Goals" || 
							val.NAME === "Match Time Result" || 
							val.NAME === "Half Time/Full Time"
						) ||
						// Basketball
						(sport === 18 &&
							[
								"Odd/Even",
								"Race to",
								"Winning Margin"
						 	].some(a => val.NAME?.includes(a)) ||
							val.NAME === "Double Result" ||
							val.NAME === "1st Half"
						) ||
						//Baseball
						(sport === 16 &&
							[
								"Winning Margin"
						 	].some(a => val.NAME.includes(a)) ||
							val.NAME === "Match Correct Score" ||
							val.NAME === "Alternative Money Line and Total " 
						) || 
						// Hockey
						(sport === 17 &&
							[
								"Correct Score",
								"Both teams to score at least",
								"Alternative Puck Lines"
							].some(a =>val.NAME.includes(a)) ||
							val.NAME === "Game Lines"
							
						) ||
						// All Sports
						[
							"Team Totals"
						].some(a =>val.NAME?.includes(a))
					)
				){

					market = {}

					return
				}

				// If generic
				else{

					let params = {
						mg: marketGroup,
						id: val.ID,
						name: val.NAME,
						suspend: val["SUCCESS, SUSPENDED"],
						order: val.ORDER,
						participants: transformParticipants(market,id, val.ID)
					}
					
					marketGroup = transformMarketGroup(params)

					//Looping and replacing a new ID in Participants
					marketGroup[val.ID].participants = newIdInParticipants(marketGroup[val.ID], val.ID)
					
					// Clean market object for the next market group
					market = {}
				}        
			break

		}
			
	})

	return marketGroup
}

function transformParticipants(data){

	let marketArr = Object.values(data)
	let results = {}

	//Looping Market
	marketArr.forEach(val => {

		if(!val.participants){ 
			return
		}
		
		let participantsArr = Object.values(val.participants)

		//Looping Participants
		participantsArr.forEach(pval => {
				
			let pId = pval.ID
			let pName = pval.NAME
			let pOdds = pval.ODDS
			let pSuspend = pval?.['SUCCESS, SUSPENDED']
			let valueEu = (eval(pOdds) + 1).toFixed(3)
			let handicap = pval?.HANDICAP
			let transformedName =  pName ? pName : val?.NAME
			
			results = {
				...results,
				[pId]:{
					id:pId,
					name: transformedName,
					short_name: transformedName,
					value_eu: valueEu === "NaN" ? "" : removeTrailingZeros(valueEu).toString(),
					suspend: pSuspend,
					handicap: handicap && handicap.replace("+",""),
				}
			}
		})
	})

	return results
}

function transformParticipantsIfNoMarket(data){

	let participants = Object.values(data)

	participants = participants.map(a => {
		let valueEu = (eval(a.ODDS) + 1).toFixed(3)

		return{
			id:a.ID,
			name: a.NAME,
			short_name: a.NAME,
			value_eu: valueEu === "NaN" ? "" : removeTrailingZeros(valueEu).toString(),
			suspend: a?.['SUCCESS, SUSPENDED'],
			handicap: a.HANDICAP && a.HANDICAP.replace("+",""),
		}
	})

	return Object.assign({}, participants)

}

function newIdInParticipants(marketGroup, mgId){

	let participantsArr = Object.values(marketGroup.participants)

	let newParticipantsID = {}

	participantsArr.map((pval, pid) => {
											
		let oldId = pval.id
		let participantsVal = pval

		newId = `${id}${mgId}${pid}`

		// If no Id in Participants then do not add in Participants
		if(oldId === "") return 

		participantsVal.id = newId

		newParticipantsID = {
			...newParticipantsID,
			[newId]:{...participantsVal}
		}
			
	})

	return newParticipantsID
}

function removeTrailingZeros(num) {

	let value = num.toString();
	const indexOfDot = value.indexOf('.');

	if (indexOfDot === -1) {
		return num;
	}

	let i = value.length - 1;

	while (["0","."].includes(value[i]) && i > indexOfDot - 1) {
		value = value.substring(0, i);
		i--;
	}

	return +value;
}

function transformMarketGroup({mg, id, name, suspend, order, participants}){

	return {
		...mg,
		[id]:{
			id:id,
			name:name,
			short_name:name,
			suspend:suspend,
			order:order,
			participants:participants
		}
	}

}