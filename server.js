var Koa = require('koa');
require('dotenv').config()

var app = new Koa();

var router = require('./routes/start.js')

app
  .use(router.routes())

  app.listen(process.env.APP_PORT);